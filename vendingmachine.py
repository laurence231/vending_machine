class vendingMachine(object):
    def __init__(self, cola_stock, choco_stock, crisp_stock, nickels, dimes ,quarters, balance = 0,change = 0):
        self.balance = balance
        self.change = change
        self.cola_stock = cola_stock
        self.crisp_stock = crisp_stock
        self.choco_stock = choco_stock
        self.nickels = nickels
        self.dimes = dimes
        self.quarters = quarters

    def Take_coin(self, coins):
        if coins == 0.05:
            self.balance += 0.05
            self.nickels += 1
        elif coins == 0.1:
            self.balance += 0.1
            self.dimes += 1
        elif coins == 0.25:
            self.balance += 0.25
            self.quarters += 1
        else:
            print('Not valid coins')
            self.change += coins
        return self.balance

    def buy_product(self,product,quantity):
        inventory = {'cola':[1,self.cola_stock], 'crisp':[0.5,self.crisp_stock],'chocolate':[0.65,self.choco_stock]}
        price = inventory[product][0]
        cost = price*quantity
        stock = inventory[product][1]
        if stock >= quantity and self.balance >= cost:
            inventory[product][1] -= quantity
            self.change = self.balance - cost
            self.balance = 0
            if product == 'cola': self.cola_stock -= quantity
            if product == 'crisp': self.crisp_stock -= quantity
            if product == 'chocolate': self.choco_stock -= quantity
        elif self.balance < cost:
            print('not enough balance')
            self.change = self.balance
            self.balance = 0
        else:
            print('sorry out of stock')
            self.change = self.balance
            self.balance = 0
        return self.change

    def return_coins(self):
        self.change = self.balance
        self.balance = 0
        return self.change

    def exact_change(self, product, quantity, nickels_in, dimes_in, quarters_in):
        price_list = {'cola':1, 'crisp':0.5, 'chocolate':0.65}
        cost = quantity*price_list[product]
        input_money = 0.05*nickels_in + 0.1*dimes_in + 0.25*quarters_in
        if input_money > cost:
            change_amount = input_money - cost
            if change_amount == 0.05:
                change_available = self.nickels*0.05
            if 0.1 <= change_amount < 0.25:
                change_available = self.nickels*0.05 + self.dimes*0.1
            if change_amount >= 0.25:
                change_available = self.nickels*0.05 + self.dimes*0.1 + self.quarters*0.25
            if change_available >= change_amount:
                print('There is change, you can purchase!!')
            else:
                print('There is not enough change in the machine, exact change only please!')


